document.addEventListener("DOMContentLoaded", function() { 
    let selectedTab = null;
    switch (window.location.hash) {
    case "#pending":
        selectedTab = document.querySelector("#pending-review-tab");
        break;
    case "#accepted":
        selectedTab = document.querySelector("#accepted-tab");
        break;
    case "#declined":
        selectedTab = document.querySelector("#declined-tab");
        break;
    }

    if (selectedTab) {
        /* eslint-disable no-undef */
        new bootstrap.Tab(selectedTab).show();
    }
});