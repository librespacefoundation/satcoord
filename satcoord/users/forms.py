from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.forms import EmailField


class RadioAmateurCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges.
    """

    template_name = "registration/forms/register.html"
    auto_id = "register_%s"

    def __init__(self, *args, **kwargs):
        if not kwargs.get("auto_id"):
            kwargs["auto_id"] = self.auto_id
        super().__init__(*args, **kwargs)

    class Meta:
        model = get_user_model()
        fields = ("callsign", "email", "first_name", "last_name")
        field_classes = {"email": EmailField}
