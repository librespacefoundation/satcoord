import factory
from faker import Faker

from satcoord.users.models import RadioAmateur, SatCoordPanel

fake1 = Faker()
profile1 = fake1.simple_profile()

fake2 = Faker()
profile2 = fake2.simple_profile()


class RadioAmateurFactory(factory.django.DjangoModelFactory):
    """Factory class for RadioAmateurs"""
    callsign = factory.Sequence(lambda n: f"{profile1['username']}{n}")
    email = factory.LazyAttribute(lambda obj: f"{obj.callsign}@example.com")
    first_name = profile1["name"].split(" ")[0]
    last_name = profile1["name"].split(" ")[1]

    class Meta:
        model = RadioAmateur


class SatCoordPanelFactory(factory.django.DjangoModelFactory):
    """Factory class for SatCoord Panel Members"""
    callsign = factory.Sequence(lambda n: f"{profile2['username']}{n}")
    email = factory.LazyAttribute(lambda obj: f"{obj.callsign}@example.com")
    first_name = profile2["name"].split(" ")[0]
    last_name = profile2["name"].split(" ")[1]

    class Meta:
        model = SatCoordPanel
