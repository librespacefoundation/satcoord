from pytest_factoryboy import register

from satcoord.users.tests.factories import RadioAmateurFactory

register(RadioAmateurFactory)
