# pylint: skip-file
import pytest
from django.contrib.auth.models import Group
from django.core import mail

from satcoord.coordinations.models import Coordination
from satcoord.users.models import RadioAmateur, SatCoordPanel


@pytest.mark.django_db
def test_user_create():
    usr = RadioAmateur.objects.create(
        callsign="test", email="test@EXAMPLE.COM", first_name="First", last_name="Last"
    )
    assert not usr.is_staff
    assert not usr.is_superuser
    assert usr.email == "test@example.com"
    assert RadioAmateur.objects.all().count() == 1
    assert SatCoordPanel.objects.all().count() == 0
    assert usr.get_full_name() == "First Last"
    assert usr.get_short_name() == "First"


@pytest.mark.django_db
def test_user_missing_callsign():
    with pytest.raises(ValueError):
        usr = RadioAmateur.objects.create(callsign="   ", email="test")


@pytest.mark.django_db
def test_user_missing_email():
    with pytest.raises(ValueError):
        usr = RadioAmateur.objects.create(callsign="test", email="   ")


@pytest.mark.django_db
def test_superuser_create():
    usr = RadioAmateur.objects.create_superuser(callsign="test", email="test@EXAMPLE.COM")
    assert usr.is_staff
    assert usr.is_superuser
    assert RadioAmateur.objects.all().count() == 1
    assert SatCoordPanel.objects.all().count() == 1


@pytest.mark.django_db
def test_superuser_nostaff():
    with pytest.raises(ValueError):
        usr = RadioAmateur.objects.create_superuser(
            callsign="test", email="test@example.com", is_staff=False
        )


@pytest.mark.django_db
def test_superuser_nosuperuser():
    with pytest.raises(ValueError):
        usr = RadioAmateur.objects.create_superuser(
            callsign="test", email="test@example.com", is_superuser=False
        )


@pytest.mark.django_db
def test_satcoordpanel_create():
    usr = SatCoordPanel.objects.create(callsign="test", email="test@example.com")
    assert usr.is_staff
    assert usr.is_superuser
    assert RadioAmateur.objects.all().count() == 1
    assert SatCoordPanel.objects.all().count() == 1


@pytest.mark.django_db
def test_satcoordpanel_default_group():
    usr = SatCoordPanel.objects.create(callsign="test", email="test@example.com")
    assert Group.objects.filter(name="Panel Members").exists()
    satcoord_panel_group = Group.objects.get(name="Panel Members")
    assert satcoord_panel_group.user_set.filter(id=usr.id).exists()
    for perm_tuple in Coordination._meta.permissions:
        assert usr.has_perm(perm_tuple[0])


@pytest.mark.django_db
def test_sendmail(radio_amateur_factory):
    usr = radio_amateur_factory.create()
    subject = "Test subject"
    body = "Test body"
    from_email = "satcoord@example.com"
    usr.email_user(subject, body, from_email)
    assert len(mail.outbox) == 1
    assert len(mail.outbox[0].to) == 1
    assert mail.outbox[0].to[0] == usr.email
    assert mail.outbox[0].subject == subject
    assert mail.outbox[0].body == body
    assert mail.outbox[0].from_email == from_email
