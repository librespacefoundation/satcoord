from django.apps import AppConfig


class UsersConfig(AppConfig):
    """App config for users"""
    default_auto_field = "django.db.models.BigAutoField"
    name = "satcoord.users"
    verbose_name = "Users"
