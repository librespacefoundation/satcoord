from django import template
from django.contrib.auth.forms import AuthenticationForm

from satcoord.users.forms import RadioAmateurCreationForm

register = template.Library()


@register.simple_tag
def registration_form(id_prefix=""):
    """Tag to render the registration form"""
    return RadioAmateurCreationForm(auto_id=f"{id_prefix}{RadioAmateurCreationForm.auto_id}")


@register.inclusion_tag('registration/forms/login.html')
def login_form(id_prefix=""):
    """Tag to render the login form"""
    return {"form": AuthenticationForm(), "id_prefix": id_prefix}
