from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views import generic

from satcoord.users.forms import RadioAmateurCreationForm


class RegisterView(generic.CreateView):
    """View for registering new users"""
    form_class = RadioAmateurCreationForm
    success_url = reverse_lazy("home")
    template_name = "registration/register.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.success_url)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Creates a new user and logs them in.
        """
        form = self.get_form()
        self.object = None  # pylint: disable=attribute-defined-outside-init
        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(
                callsign=form.cleaned_data['callsign'],
                password=form.cleaned_data['password1'],
            )
            login(request, new_user)
            return HttpResponseRedirect(self.success_url)
        return self.form_invalid(form)
