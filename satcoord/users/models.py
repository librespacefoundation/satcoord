"""
The models & managers for users
"""

from django.apps import apps
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models, transaction
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from satcoord.users.utils import get_or_create_panel_members_group


class RadioAmateurManager(BaseUserManager):
    """A model manager for RadioAmateur models"""

    def create(self, *args, **kwargs):
        """Proxy to create_user"""
        return self.create_user(*args, **kwargs)

    def _create_user(self, callsign, email, password, **extra_fields):
        """Creates and saves a RadioAmateur"""
        callsign = callsign.strip()
        email = email.strip()

        if not email:
            raise ValueError('Users must have an email address')
        if not callsign:
            raise ValueError('Users must have a callsign')

        global_user_model = apps.get_model(
            self.model._meta.app_label, self.model._meta.object_name
        )
        callsign = global_user_model.normalize_username(callsign)
        email = self.normalize_email(email)
        user = self.model(callsign=callsign, email=email, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, callsign, email, password=None, **extra_fields):
        """Creates a new user"""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(callsign, email, password, **extra_fields)

    def create_superuser(self, callsign, email, password=None, **extra_fields):
        """Creates a new superuser"""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        user = self._create_user(callsign, email, password, **extra_fields)

        panel_members_group = get_or_create_panel_members_group()
        panel_members_group.user_set.add(user)

        return user


class SatCoordPanelManager(RadioAmateurManager):
    """Model manager for SatCoord Panel members"""

    def create_user(self, callsign, email, password=None, **extra_fields):
        return super().create_superuser(callsign, email, password, **extra_fields)

    def get_queryset(self):
        """Satcoord Panel members have staff=True"""
        return super().get_queryset().filter(is_staff=True)


class RadioAmateur(AbstractBaseUser, PermissionsMixin):
    """
    A user class representing a radio amateur.
    """

    username_validator = UnicodeUsernameValidator()

    callsign = models.CharField(
        _("callsign"),
        max_length=150,
        unique=True,
        help_text=_("Required."),
        validators=[username_validator],
        error_messages={
            "unique": _("A user with that callsign already exists."),
        },
    )
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(
        _("email address"),
        blank=False,
        unique=True,
        help_text=_("Required."),
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = RadioAmateurManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "callsign"
    REQUIRED_FIELDS = ["email"]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f"{self.first_name} {self.last_name}"
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class SatCoordPanel(RadioAmateur):
    """Represents members of the Satellite Coordination Panel"""

    objects = SatCoordPanelManager()

    class Meta:
        proxy = True
        verbose_name = "Satcoord Panel Member"

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_staff = True
            self.is_superuser = True
            with transaction.atomic():
                super().save(*args, **kwargs)
                self.refresh_from_db()
                panel_members_group = get_or_create_panel_members_group()
                panel_members_group.user_set.add(self)
        super().save(*args, **kwargs)
