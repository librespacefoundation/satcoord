# pylint: skip-file

from django.test import Client
from django.urls import reverse


def test_robots():
    client = Client()
    resp = client.get(reverse("robots"))
    assert "Disallow: /" in str(resp.content)
    assert "user-agent: *" in str(resp.content)