# pylint: skip-file

import pytest
from django.test import Client
from django.urls import reverse

from satcoord.coordinations.forms import ReviewForm
from satcoord.coordinations.models import Beam, Coordination, Revision
from satcoord.coordinations.tests.test_models import test_owner_and_perms


@pytest.mark.django_db
def test_new_coord(coordination_factory, revision_factory, beam_factory, radio_amateur_factory):
    coord = coordination_factory.build(operator_responsible=None)
    rev = revision_factory.build()
    beam = beam_factory.build()

    client = Client()
    get_response_unauth = client.get(reverse("coordination-new"))
    assert get_response_unauth.status_code == 302

    radio_amateur = radio_amateur_factory()
    client.force_login(radio_amateur)

    get_response = client.get(reverse("coordination-new"))
    assert get_response.status_code == 200

    post_data = {
        "mission_name": coord.mission_name,
        "supporting_organization": coord.supporting_organization,
        "description": coord.description,
        "file": rev.file,
        "beams-TOTAL_FORMS": 1,
        "beams-INITIAL_FORMS": 0,
        "beams-MIN_NUM_FORMS": 0,
        "beams-MAX_NUM_FORMS": 1000,
        "beams-0-direction": beam.direction,
        "beams-0-modulation": beam.modulation,
        "beams-0-band": beam.band,
        "beams-0-service_area": beam.service_area,
        "beams-0-description": beam.description
    }

    post_response = client.post(reverse("coordination-new"), data=post_data)
    assert post_response.status_code == 302

    assert Coordination.objects.count() == 1
    coord_instance = Coordination.objects.latest("id")
    assert coord_instance.operator_responsible_id == radio_amateur.id
    assert Revision.objects.count() == 1
    rev1_instance = Revision.objects.latest("id")
    assert rev1_instance == coord_instance.get_latest_revision()
    assert Beam.objects.count() == 1
    beam_instance = Beam.objects.latest("id")
    assert beam_instance.coordination_id == coord_instance.id

    redirect_get_response = client.get(post_response.url)
    assert redirect_get_response.status_code == 200

    same_mission_name_data = {
        "mission_name": coord.mission_name,
        "supporting_organization": coord.supporting_organization,
        "description": coord.description,
        "file": rev.file,
        "beams-TOTAL_FORMS": 0,
        "beams-INITIAL_FORMS": 0,
        "beams-MIN_NUM_FORMS": 0,
        "beams-MAX_NUM_FORMS": 1000,
    }

    bad_post_response = client.post(reverse("coordination-new"), data=same_mission_name_data)
    assert bad_post_response.status_code == 400


@pytest.mark.django_db
def test_new_coord_http_methods(radio_amateur_factory):
    client = Client()
    radio_amateur = radio_amateur_factory()
    client.force_login(radio_amateur)
    opt_resp = client.options(reverse("coordination-new"))
    assert opt_resp.status_code == 200

    patch_resp = client.patch(reverse("coordination-new"), data={})
    assert patch_resp.status_code == 405


test_owner_and_perms


@pytest.mark.django_db
def test_coord_detail_view_revisions(
    coordination_factory, revision_factory, radio_amateur_factory, sat_coord_panel_factory
):
    usr_owner, usr_not_owner = radio_amateur_factory.create_batch(2)
    panel_member = sat_coord_panel_factory.create()

    coord = coordination_factory.create(operator_responsible=usr_owner)
    revision_factory(coordination=coord)

    client = Client()
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == False
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == False

    client.force_login(usr_not_owner)
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == False
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == False
    client.logout()

    client.force_login(usr_owner)
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == True
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == False
    client.logout()

    client.force_login(panel_member)
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == True
    assert resp.context["show_review_tools"] == True
    assert resp.context["show_revise_tools"] == False

    coord.needs_changes("comment")
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == True
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == False
    client.logout()

    client.force_login(usr_owner)
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == True
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == True
    client.logout()

    client.force_login(usr_not_owner)
    resp = client.get(coord.get_absolute_url())
    assert resp.context["show_revisions"] == False
    assert resp.context["show_review_tools"] == False
    assert resp.context["show_revise_tools"] == False


@pytest.mark.django_db
def test_review_revise(
    coordination_factory, beam_factory, revision_factory, radio_amateur_factory,
    sat_coord_panel_factory
):
    user = radio_amateur_factory.create()
    panel_member = sat_coord_panel_factory.create()
    coord = coordination_factory(operator_responsible=user)
    beams = beam_factory.create_batch(2, coordination=coord)
    rev1 = revision_factory(coordination=coord)

    assert coord.is_reviewable()

    client = Client()
    client.force_login(panel_member)

    post_data = {"verdict": ReviewForm.NEEDS_CHANGES, "comments": ""}

    failed_resp = client.post(
        reverse("coordination-review", kwargs={"coord_id": coord.id}), data=post_data
    )
    assert failed_resp.status_code == 400

    post_data["comments"] = "Comment"
    review_resp1 = client.post(
        reverse("coordination-review", kwargs={"coord_id": coord.id}), data=post_data
    )
    assert review_resp1.status_code == 302
    assert review_resp1.url == coord.get_absolute_url()
    rev1.refresh_from_db()
    assert rev1.comments == "Comment"
    resp_forbidden0 = client.post(
        reverse("coordination-review", kwargs={"coord_id": coord.id}), data={}
    )
    assert resp_forbidden0.status_code == 403
    client.logout()

    assert coord.can_user_revise(user)
    client.force_login(user)
    post_data = {
        "mission_name": coord.mission_name + "_revised",
        "supporting_organization": coord.supporting_organization + "_revised",
        "description": coord.description + "_revised",
        "file": rev1.file,
        "beams-TOTAL_FORMS": 2,
        "beams-INITIAL_FORMS": 2,
        "beams-MIN_NUM_FORMS": 0,
        "beams-MAX_NUM_FORMS": 1000,
        "beams_to_delete": 1
    }

    def get_beam_as_formdata(beam, idx):
        dct = {}
        for field in ("id", "coordination", "direction", "modulation", "band", "service_area",
                      "description"):
            if field != "coordination":
                dct[f"beams-{idx}-{field}"] = getattr(beam, field)
            else:
                dct[f"beams-{idx}-{field}"] = beam.coordination_id
        return dct

    for idx, beam in enumerate(beams):
        post_data.update(get_beam_as_formdata(beam, idx))

    post_data["beams-1-service_area"] = "revised"

    revise_resp = client.post(
        reverse("coordination-revise", kwargs={"coord_id": coord.id}), data=post_data
    )

    assert revise_resp.status_code == 302
    assert revise_resp.url == coord.get_absolute_url() + "?back=overview"
    beams = Beam.objects.all()
    assert beams.count() == 1
    beam = beams[0]
    assert beam.service_area == "revised"

    resp_forbidden1 = client.post(
        reverse("coordination-revise", kwargs={"coord_id": coord.id}), data={}
    )
    assert resp_forbidden1.status_code == 403
    resp_forbidden2 = client.post(
        reverse("coordination-review", kwargs={"coord_id": coord.id}), data={}
    )
    assert resp_forbidden2.status_code == 403


@pytest.mark.django_db
def test_overview_dashboards(
    coordination_factory, revision_factory, radio_amateur_factory, sat_coord_panel_factory
):

    panel_member = sat_coord_panel_factory.create()
    client = Client()

    anonymous_resp = client.get(reverse("overview"))
    assert anonymous_resp.status_code == 302

    client.force_login(panel_member)
    panel_overview_resp = client.get(reverse("overview"))
    assert panel_overview_resp.context["has_new_coords"] == False
    assert panel_overview_resp.context["has_unaddressed_coords"] == False

    user = radio_amateur_factory.create()
    coord = coordination_factory(operator_responsible=user)
    revision_factory(coordination=coord)
    panel_overview_resp = client.get(reverse("overview"))
    assert panel_overview_resp.context["has_new_coords"] == True
    assert panel_overview_resp.context["has_unaddressed_coords"] == False
    client.logout()

    client.force_login(user)
    user_resp = client.get(reverse("overview"))
    assert user_resp.context["has_coords"] == True
    assert user_resp.context["has_need_change"] == False
    assert user_resp.context["has_pending_review"] == True
    assert user_resp.context["has_accepted"] == False
    assert user_resp.context["has_declined"] == False

    coord.needs_changes("comment")
    user_resp = client.get(reverse("overview"))
    assert user_resp.context["has_coords"] == True
    assert user_resp.context["has_need_change"] == True
    assert user_resp.context["has_pending_review"] == False
    assert user_resp.context["has_accepted"] == False
    assert user_resp.context["has_declined"] == False

    revision_factory(coordination=coord)
    user_resp = client.get(reverse("overview"))
    assert user_resp.context["has_coords"] == True
    assert user_resp.context["has_need_change"] == False
    assert user_resp.context["has_pending_review"] == True
    assert user_resp.context["has_accepted"] == False
    assert user_resp.context["has_declined"] == False
    client.logout()

    client.force_login(panel_member)
    panel_overview_resp = client.get(reverse("overview"))
    assert panel_overview_resp.context["has_new_coords"] == False
    assert panel_overview_resp.context["has_unaddressed_coords"] == True
    coord.accept()
    assert not coord.get_latest_revision().comments
    panel_overview_resp = client.get(reverse("overview"))
    assert panel_overview_resp.context["has_new_coords"] == False
    assert panel_overview_resp.context["has_unaddressed_coords"] == False
    client.logout()

    client.force_login(user)
    user_resp = client.get(reverse("overview"))
    assert user_resp.context["has_coords"] == True
    assert user_resp.context["has_need_change"] == False
    assert user_resp.context["has_pending_review"] == False
    assert user_resp.context["has_accepted"] == True
    assert user_resp.context["has_declined"] == False
