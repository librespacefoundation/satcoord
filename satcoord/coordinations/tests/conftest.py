from pytest_factoryboy import register

from satcoord.coordinations.tests.factories import BeamFactory, CoordinationFactory, \
    RevisionFactory
from satcoord.users.tests.factories import RadioAmateurFactory, SatCoordPanelFactory

register(BeamFactory)
register(CoordinationFactory)
register(RevisionFactory)
register(RadioAmateurFactory)
register(SatCoordPanelFactory)
