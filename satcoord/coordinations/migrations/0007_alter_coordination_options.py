# Generated by Django 4.1.3 on 2023-02-14 11:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('coordinations', '0006_alter_revision_revision_number'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='coordination',
            options={'permissions': [('view_revisions', "Can view the coordination's revisions"), ('review_coordinations', 'Can review a coordination')]},
        ),
    ]
