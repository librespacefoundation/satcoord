from django.urls import path

from satcoord.coordinations.views import CoordinationDetailView, CoordinationListView, \
    new_coordination_view, review_coordination_view, revise_coordination_view

urlpatterns = [
    path("", CoordinationListView.as_view(), name="coordination-list"),
    path("new/", new_coordination_view, name="coordination-new"),
    path('<int:pk>/', CoordinationDetailView.as_view(), name='coordination-detail'),
    path('<int:coord_id>/review/', review_coordination_view, name='coordination-review'),
    path('<int:coord_id>/revise/', revise_coordination_view, name='coordination-revise'),
]
