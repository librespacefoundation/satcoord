from django.contrib import admin

from satcoord.coordinations.models import Beam, Coordination, Revision


@admin.register(Coordination)
class CoordinationAdmin(admin.ModelAdmin):
    """Admin class for Coordinations"""
    ordering = ("-id", )
    list_display = ("id", "mission_name", "status", "date", "operator_responsible")


@admin.register(Beam)
class BeamAdmin(admin.ModelAdmin):
    """Admin class for Beams"""

    ordering = ("-coordination__id", "-id")


@admin.register(Revision)
class RevisionAdmin(admin.ModelAdmin):
    """Admin class for Revisions"""

    ordering = ("-coordination__id", "-id")
