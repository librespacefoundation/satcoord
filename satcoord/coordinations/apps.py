from django.apps import AppConfig


class CoordinationsConfig(AppConfig):
    """App config for coordinations app"""
    default_auto_field = "django.db.models.BigAutoField"
    name = "satcoord.coordinations"
