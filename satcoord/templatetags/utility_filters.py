from django import template

register = template.Library()


@register.filter(is_safe=True)
def js_bool(value):
    """Displays boolean values as lowercase strings"""
    return "true" if value else "false"


@register.filter(is_safe=True)
def js_bool_negated(value):
    """Displays boolean values as lowercase strings after negating the result"""
    return "true" if not value else "false"
