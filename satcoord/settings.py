"""SatCoord Application django settings

For local development, edit the .env.dev file.
For production deployment, edit the ,env.prod file.
"""

import os
from pathlib import Path

from decouple import AutoConfig as DecoupleConfig
from decouple import Csv
from dj_database_url import parse as db_url

BASE_DIR = Path(__file__).resolve().parent.parent

PRODUCTION_DOTENV = ".env.prod"
DEVELOPMENT_DOTENV = ".env.dev"
ENVIRONMENT = os.environ.get("ENVIRONMENT") or "dev"
if ENVIRONMENT == "production":
    config = DecoupleConfig(search_path=PRODUCTION_DOTENV)
else:
    config = DecoupleConfig(search_path=DEVELOPMENT_DOTENV)

DEBUG = config('DEBUG', default=False, cast=bool)

# Application definition
DJANGO_APPS = [
    "django.contrib.admin", "django.contrib.auth", "django.contrib.contenttypes",
    "django.contrib.sessions", "django.contrib.messages", "django.contrib.staticfiles",
    'django.contrib.sites'
]

THIRD_PARTY_APPS = ["widget_tweaks", "compressor"]

DEV_APPS = ["debug_toolbar", "django_extensions"]

LOCAL_APPS = ["satcoord", "satcoord.users", "satcoord.coordinations"]

if DEBUG:
    DJANGO_APPS += DEV_APPS
    DEBUG_TOOLBAR_CONFIG = {'SHOW_TOOLBAR_CALLBACK': lambda _: True}
else:
    DEBUG_TOOLBAR_CONFIG = False

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

if DEBUG:
    MIDDLEWARE = ['debug_toolbar.middleware.DebugToolbarMiddleware'] + MIDDLEWARE

ROOT_URLCONF = "satcoord.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(BASE_DIR, "satcoord", "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "satcoord.wsgi.application"

# Database
DATABASE_URL = config('DATABASE_URL', default='sqlite:///db.sqlite3')
DATABASES = {'default': db_url(DATABASE_URL)}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Internationalization
LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_ROOT = config('STATIC_ROOT', default=Path('staticfiles').resolve())
STATIC_URL = config('STATIC_URL', default='/static/')
STATICFILES_DIRS = [os.path.join(BASE_DIR, "satcoord", "static")]
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)
MEDIA_ROOT = config('MEDIA_ROOT', default=Path('media').resolve())
MEDIA_URL = config('MEDIA_URL', default='/media/')
# Compress (Use COMPRESS_ENABLED & COMPRESS_OFFLINE in production)
COMPRESS_ENABLED = config('COMPRESS_ENABLED', default=False, cast=bool)
COMPRESS_OFFLINE = config('COMPRESS_OFFLINE', default=False, cast=bool)
COMPRESS_FILTERS = {
    'css': [
        'compressor.filters.css_default.CssAbsoluteFilter',
        'compressor.filters.cssmin.CSSMinFilter',
    ],
    'js': [
        'compressor.filters.jsmin.JSMinFilter',
    ],
}

# Default primary key field type
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

# Security
SECRET_KEY = config('SECRET_KEY', default='changeme')
ALLOWED_HOSTS = config('ALLOWED_HOSTS', default='localhost', cast=Csv())
AUTH_USER_MODEL = "users.RadioAmateur"
SECURE_HSTS_SECONDS = config('SECURE_HSTS_SECONDS', default=31536000, cast=int)
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

LOGIN_REDIRECT_URL = "/"

# Email
EMAIL_HOST = config('EMAIL_HOST', default='localhost')
EMAIL_PORT = config('EMAIL_PORT', default=25, cast=int)
EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='')
EMAIL_USE_TLS = config('EMAIL_USE_TLS', default=False, cast=bool)
EMAIL_USE_SSL = config("EMAIL_USE_SSL", default=False)
EMAIL_TIMEOUT = config('EMAIL_TIMEOUT', default=300, cast=int)
EMAIL_SSL_KEYFILE = config("EMAIL_SSL_KEYFILE", default=None)
EMAIL_SSL_CERTFILE = config("EMAIL_SSL_CERTFILE", default=None)
DEFAULT_FROM_EMAIL = config("DEFAULT_FROM_EMAIL", default=None)

if ENVIRONMENT == "production":
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SITE_ID = 1
