FROM python:3.9.2

WORKDIR /usr/local/src/satcoord/

ENV POETRY_HOME=/opt/poetry

RUN groupadd -r satcoord \
    && useradd -r -g satcoord satcoord

RUN apt-get update \
    && apt-get -y install libpq-dev

# Copy the pyproject.toml file and install dependencies
COPY pyproject.toml /usr/local/src/satcoord/
RUN python3 -m venv ${POETRY_HOME} \
    && ${POETRY_HOME}/bin/pip install poetry==1.3.2 \
    && ${POETRY_HOME}/bin/poetry --version \
    && ${POETRY_HOME}/bin/poetry install --no-interaction 

# Copy the rest of the project files
COPY . /usr/local/src/satcoord/

# Set the path to include Poetry's binaries
ENV PATH="${PATH}:${POETRY_HOME}/bin"
